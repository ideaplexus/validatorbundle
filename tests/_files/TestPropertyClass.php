<?php

namespace IPC\Tests\ValidatorBundle\_files;

class TestPropertyClass
{
    protected $propertyOne;
    protected $propertyTwo;
    public function setPropertyOne($value) {
        $this->propertyOne = $value;
        return $this;
    }
    public function setPropertyTwo($value) {
        $this->propertyTwo = $value;
        return $this;
    }
    public function __get($name) {
        $value = null;
        if(property_exists($this, $name))
        {
            $value = $this->$name;
        }
        return $value;
    }
}
