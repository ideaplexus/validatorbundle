<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\DateRange;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\DateRange
 */
class DateRangeTest extends TestCase
{
    /**
     * @var DateRange
     */
    protected $constraint;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new DateRange();
    }

    /**
     * @return void
     */
    public function testProperties(): void
    {
        $this->assertEquals('startDate', $this->constraint->startDate);
        $this->assertEquals('endDate', $this->constraint->endDate);
        $this->assertEquals(false, $this->constraint->skipInvalid);
        $this->assertEquals('{{startDate}} and {{endDate}} are not a valid date range.', $this->constraint->message);
    }

    /**
     * @return array
     */
    public function provider__constructException(): array
    {
        return [
            [
                'options' => [
                    DateRange::OPTION_START_DATE => '',
                ],
                'message' => 'startDate has to be a (non-empty) string.',
            ],
            [
                'options' => [
                    DateRange::OPTION_START_DATE => 1234,
                ],
                'message' => 'startDate has to be a (non-empty) string.',
            ],
            [
                'options' => [
                    DateRange::OPTION_END_DATE => '',
                ],
                'message' => 'endDate has to be a (non-empty) string.',
            ],
            [
                'options' => [
                    DateRange::OPTION_END_DATE => 1234,
                ],
                'message' => 'endDate has to be a (non-empty) string.',
            ],
            [
                'options' => [
                    DateRange::OPTION_SKIP_INVALID => '',
                ],
                'message' => 'skipInvalid has to be a boolean.',
            ],
            [
                'options' => [
                    DateRange::OPTION_SKIP_INVALID => 1,
                ],
                'message' => 'skipInvalid has to be a boolean.',
            ],
        ];
    }

    /**
     * @param array  $options
     * @param string $message
     *
     * @return void
     *
     * @dataProvider provider__constructException
     *
     * @covers ::__construct
     */
    public function test__constructException($options, $message): void
    {
        $this->expectException(ConstraintDefinitionException::class);
        $this->expectExceptionMessage($message);

        new DateRange($options);
    }

    /**
     * @return void
     *
     * @covers ::__construct
     */
    public function test__construct(): void
    {
        $options = [
            DateRange::OPTION_START_DATE   => 'begin',
            DateRange::OPTION_END_DATE     => 'end',
            DateRange::OPTION_SKIP_INVALID => true,
        ];

        $dateRange = new DateRange($options);

        $this->assertEquals($options[DateRange::OPTION_START_DATE], $dateRange->startDate);
        $this->assertEquals($options[DateRange::OPTION_END_DATE], $dateRange->endDate);
        $this->assertEquals($options[DateRange::OPTION_SKIP_INVALID], $dateRange->skipInvalid);
    }

    /**
     * @return void
     *
     * @covers ::getTargets
     */
    public function testGetTargets(): void
    {
        $this->assertEquals(Constraint::CLASS_CONSTRAINT, $this->constraint->getTargets());
    }
}
