<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\Tests\ValidatorBundle\_files\TestDateRangeClass;
use IPC\ValidatorBundle\Validator\Constraints\DateRange;
use IPC\ValidatorBundle\Validator\Constraints\DateRangeValidator;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\DateRangeValidator
 */
class DateRangeValidatorTest extends AbstractValidatorTest
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new DateRange();
        $this->validator  = new DateRangeValidator();
    }

    /**
     * @return array
     */
    public function providerValidValues(): array
    {
        return [
            [(new TestDateRangeClass)->setStartDate(new \DateTime())->setEndDate(new \DateTime())],
            [(new TestDateRangeClass)->setStartDate(new \DateTime())->setEndDate(new \DateTime('+1 day'))],
        ];
    }

    /**
     * @return array
     */
    public function providerInvalidValues(): array
    {
        return [
            ['a'],
            ['2'],
            [2],
            [0.25],
            [[]],
            [new \stdClass()],
            [(new TestDateRangeClass)->setStartDate(new \DateTime('+1 day'))->setEndDate(new \DateTime())],
            [(new TestDateRangeClass)->setStartDate(new \DateTime('+1 day'))->setEndDate('-1 day')],
        ];
    }

    /**
     * @param $value
     *
     * @return void
     *
     * @dataProvider providerInvalidValues
     *
     * @covers ::validate
     */
    public function testSkipInvalid($value): void
    {
        $this->constraint->skipInvalid = true;

        // skip last of provided "invalid" values, since it's not the intended "invalid" value
        if (\is_object($value) &&
            method_exists($value, 'getStartDate') &&
            method_exists($value, 'getEndDate')
        ) {
            $this->assertTrue(true);
            return;
        }

        $this->testValidateValidValues($value);
    }
}
