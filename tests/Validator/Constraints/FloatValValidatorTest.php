<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\FloatVal;
use IPC\ValidatorBundle\Validator\Constraints\FloatValValidator;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\FloatValValidator
 */
class FloatValValidatorTest extends AbstractValidatorTest
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new FloatVal();
        $this->validator = new FloatValValidator();
    }

    /**
     * @return array
     */
    public function providerValidValues(): array
    {
        return [
            ['1.0'],
            [1.234],
            [1.221212],
            [0],
            [-1.0],
            [1e7],
            ['1e7'],
            ['-1.233'],
        ];
    }

    /**
     * @return array
     */
    public function providerInvalidValues(): array
    {
        return [
            ['a'],
            [new \stdClass()],
            [true],
            ['false'],
            ['123.Test'],
            ['123,456.78'],
            [[]],
        ];
    }
}
