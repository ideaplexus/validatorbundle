<?php

namespace IPC\Tests\ValidatorBundle;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

abstract class AbstractValidatorTest extends TestCase
{
    /**
     * @var Constraint
     */
    protected $constraint;

    /**
     * @var ConstraintValidator
     */
    protected $validator;

    /**
     * @return array
     *
     * @covers ::validate
     */
    abstract public function providerValidValues(): array;

    /**
     * @param mixed $value
     *
     * @return void
     *
     * @dataProvider providerValidValues
     *
     * @covers ::validate
     */
    public function testValidateValidValues($value): void
    {
        $context = $this->createMock(ExecutionContextInterface::class);
        $context->expects($this->never())->method($this->anything());
        $this->validator->initialize($context);
        $this->validator->validate($value, $this->constraint);
    }

    /**
     * @return array
     */
    abstract public function providerInvalidValues(): array;

    /**
     * @param mixed $value
     *
     * @return void
     *
     * @dataProvider providerInvalidValues
     *
     * @covers ::validate
     */
    public function testValidateInvalidValues($value): void
    {
        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);
        $context = $this->createMock(ExecutionContextInterface::class);

        $builder
            ->expects($this->once())
            ->method('addViolation');

        $builder
            ->expects($this->any())
            ->method('atPath')
            ->willReturnSelf();

        $context
            ->expects($this->once())
            ->method('buildViolation')
            ->willReturn($builder);

        $this->validator->initialize($context);
        $this->validator->validate($value, $this->constraint);
    }
}
