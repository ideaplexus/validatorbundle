<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\BoolVal;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\BoolVal
 */
class BoolValTest extends TestCase
{
    /**
     * @var BoolVal
     */
    protected $constraint;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new BoolVal();
    }

    /**
     * @return void
     *
     *
     */
    public function testProperties(): void
    {
        $this->assertEquals('This value is not a valid boolean.', $this->constraint->message);
    }

    /**
     * @return void
     *
     * @covers ::getTargets
     */
    public function testGetTargets(): void
    {
        $this->assertEquals(Constraint::PROPERTY_CONSTRAINT, $this->constraint->getTargets());
    }
}
