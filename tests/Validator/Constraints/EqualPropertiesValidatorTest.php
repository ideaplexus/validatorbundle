<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\Tests\ValidatorBundle\_files\TestPropertyClass;
use IPC\ValidatorBundle\Validator\Constraints\EqualProperties;
use IPC\ValidatorBundle\Validator\Constraints\EqualPropertiesValidator;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\EqualPropertiesValidator
 */
class EqualPropertiesValidatorTest extends AbstractValidatorTest
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new EqualProperties(['propertyOne', 'propertyTwo']);
        $this->validator  = new EqualPropertiesValidator();
    }

    /**
     * @return array
     */
    public function providerValidValues(): array
    {
        return [
            [(new TestPropertyClass)->setPropertyOne(123)->setPropertyTwo(123)],
            [(new TestPropertyClass)->setPropertyOne('a')->setPropertyTwo('a')],
            [(new TestPropertyClass)->setPropertyOne([])->setPropertyTwo([])],
            [(new TestPropertyClass)->setPropertyOne(new \stdClass())->setPropertyTwo(new \stdClass())],
            [['propertyOne' => 123, 'propertyTwo' => 123]],
            [['propertyOne' => 'a', 'propertyTwo' => 'a']],
            [['propertyOne' => [], 'propertyTwo' => []]],
            [['propertyOne' => new \stdClass(), 'propertyTwo' => new \stdClass()]],
        ];
    }

    /**
     * @return array
     */
    public function providerInvalidValues(): array
    {
        return [
            [(new TestPropertyClass)->setPropertyOne(123)->setPropertyTwo(1234)],
            [(new TestPropertyClass)->setPropertyOne(123)->setPropertyTwo('123')],
            [(new TestPropertyClass)->setPropertyOne([0 => 'a'])->setPropertyTwo([1 => 'a'])],
        ];
    }

    /**
     * @return void
     *
     * @covers ::validate
     */
    public function testValidateSkipNull(): void
    {
        $this->constraint = new EqualProperties([
            EqualProperties::OPTION_PROPERTIES => ['propertyOne', 'propertyTwo', 'propertyThree'],
            EqualProperties::OPTION_SKIP_NULL  => true,
        ]);

        $this->testValidateValidValues(['propertyOne' => null, 'propertyTwo' => null, 'propertyThree' => 1]);
        $this->testValidateInvalidValues(['propertyOne' => null, 'propertyTwo' => null]);
        $this->testValidateInvalidValues(new TestPropertyClass);
    }

    /**
     * @return void
     *
     * @covers ::validate
     */
    public function testValidateInvert(): void
    {
        $this->constraint = new EqualProperties([
            EqualProperties::OPTION_PROPERTIES => ['propertyOne', 'propertyTwo'],
            EqualProperties::OPTION_INVERT     => true,
        ]);

        $this->testValidateInvalidValues(['propertyOne' => 123, 'propertyTwo' => 123]);
        $this->testValidateInvalidValues(new TestPropertyClass);
        $this->testValidateValidValues(['propertyOne' => 123, 'propertyTwo' => 'abc']);
    }
}
