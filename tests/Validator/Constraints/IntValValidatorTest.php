<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\IntVal;
use IPC\ValidatorBundle\Validator\Constraints\IntValValidator;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\IntValValidator
 */
class IntValValidatorTest extends AbstractValidatorTest
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new IntVal();
        $this->validator  = new IntValValidator();
    }

    /**
     * @return array
     */
    public function providerValidValues(): array
    {
        return [
            [1234],
            [-123],
            [0123],
            [0x1A],
            [0b11111111],
            ['1234'],
            ['-1234'],
            [0],
            [1],
            ['1'],
            ['0'],
        ];
    }

    /**
     * @return array
     */
    public function providerInvalidValues(): array
    {
        return [
            [1.002],
            [new \stdClass()],
            [[]],
            [false],
            ['2Foo'],
            ['']
        ];
    }
}
