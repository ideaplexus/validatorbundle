<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IntValValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        if (\is_int($value)) {
            return;
        }

        if (\is_string($value)) {
            $formatter = new \NumberFormatter('en_US', \NumberFormatter::TYPE_INT32);
            if (\is_numeric($value) && $formatter->parse($value) !== false) {
                return;
            }
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
