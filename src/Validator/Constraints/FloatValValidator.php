<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class FloatValValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        if (\is_float($value) || \is_int($value) ) {
            return;
        }

        if (\is_string($value)) {
            $formatter = new \NumberFormatter('en_US', \NumberFormatter::TYPE_DEFAULT);
            if (\is_numeric($value) && $formatter->parse($value) !== false) {
                return;
            }
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
