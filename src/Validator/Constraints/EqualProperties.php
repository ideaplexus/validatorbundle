<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class EqualProperties extends Constraint
{
    public const MESSAGE_NO_PROPERTIES = 'Not properties where configured.';
    public const MESSAGE_EQUAL         = 'The given properties are not equal to each other.';
    public const MESSAGE_EQUAL_INVERT  = 'The given properties are equal to each other.';

    public const OPTION_PROPERTIES = 'properties';
    public const OPTION_INVERT     = 'invert';
    public const OPTION_SKIP_NULL  = 'skipNull';

    /**
     * @var string
     */
    public $message;

    /**
     * @var array
     */
    public $properties = [];

    /**
     * @var bool
     */
    public $invert = false;

    /**
     * @var bool
     */
    public $skipNull = false;

    /**
     * @param array|null $options
     *
     * @throws ConstraintDefinitionException
     * @throws InvalidOptionsException
     * @throws MissingOptionsException
     */
    public function __construct($options = null)
    {
        $optionKeys = [
            'groups',
            'message',
            self::OPTION_PROPERTIES,
            self::OPTION_INVERT,
            self::OPTION_SKIP_NULL,
        ];

        if (\is_array($options) &&
            !array_intersect(array_keys($options), $optionKeys)
        ) {
            $options = [self::OPTION_PROPERTIES => $options];
        }

        parent::__construct($options);

        if (!\is_array($options[self::OPTION_PROPERTIES])) {
            throw new ConstraintDefinitionException('The option "properties" is expected to be an array in constraint '.__CLASS__);
        }

        if (\count($options[self::OPTION_PROPERTIES]) < 2) {
            throw new ConstraintDefinitionException('The option "properties" requires two or more elements in constraint '.__CLASS__);
        }

        if (isset($options[self::OPTION_INVERT])) {
            if (!\is_bool($options[self::OPTION_INVERT])) {
                throw new ConstraintDefinitionException('invert has to be a boolean.');
            }
            $this->invert = $options[self::OPTION_INVERT];
        }

        if (isset($options[self::OPTION_SKIP_NULL])) {
            if (!\is_bool($options[self::OPTION_SKIP_NULL])) {
                throw new ConstraintDefinitionException('skipNull has to be a boolean.');
            }
            $this->skipNull = $options[self::OPTION_SKIP_NULL];
        }
    }

    /**
     * @return string
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * @return array
     */
    public function getRequiredOptions(): array
    {
        return [self::OPTION_PROPERTIES];
    }
}
