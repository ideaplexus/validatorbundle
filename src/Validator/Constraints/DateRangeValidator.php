<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateRangeValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        try {
            $accessor  = PropertyAccess::createPropertyAccessor();
            $startDate = $accessor->getValue($value, $constraint->startDate);
            $endDate   = $accessor->getValue($value, $constraint->endDate);
        } catch (\Exception $e) {
            $startDate = null;
            $endDate   = null;
        }

        if ($startDate instanceof \DateTime && $endDate instanceof \DateTime) {
            $startDate = (clone $startDate)->setTime(0, 0, 0);
            $endDate   = (clone $endDate)->setTime(0, 0, 0);
            if ($startDate->getTimestamp() <= $endDate->getTimestamp()) {
                return;
            }
        } elseif ($constraint->skipInvalid) {
            return;
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation()
        ;
    }
}
