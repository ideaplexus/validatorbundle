<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class DateRange extends Constraint
{
    public const OPTION_START_DATE   = 'startDate';
    public const OPTION_END_DATE     = 'endDate';
    public const OPTION_SKIP_INVALID = 'skipInvalid';

    /**
     * @var string
     */
    public $startDate = 'startDate';

    /**
     * @var string
     */
    public $endDate = 'endDate';

    /**
     * @var bool
     */
    public $skipInvalid = false;

    /**
     * @var string
     */
    public $message = '{{startDate}} and {{endDate}} are not a valid date range.';

    /**
     * @param array|null $options
     *
     * @throws ConstraintDefinitionException
     * @throws InvalidOptionsException
     * @throws MissingOptionsException
     */
    public function __construct($options = null)
    {
        parent::__construct($options);

        if (isset($options[self::OPTION_START_DATE])) {
            if (!\is_string($options[self::OPTION_START_DATE]) || empty($options[self::OPTION_START_DATE])) {
                throw new ConstraintDefinitionException('startDate has to be a (non-empty) string.');
            }
            $this->startDate = $options[self::OPTION_START_DATE];
        }

        if (isset($options[self::OPTION_END_DATE])) {
            if (!\is_string($options[self::OPTION_END_DATE]) || empty($options[self::OPTION_END_DATE])) {
                throw new ConstraintDefinitionException('endDate has to be a (non-empty) string.');
            }
            $this->endDate = $options[self::OPTION_END_DATE];
        }

        if (isset($options[self::OPTION_SKIP_INVALID])) {
            if (!\is_bool($options[self::OPTION_SKIP_INVALID])) {
                throw new ConstraintDefinitionException('skipInvalid has to be a boolean.');
            }
            $this->skipInvalid = $options[self::OPTION_SKIP_INVALID];
        }
    }

    /**
     * @return string
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
