<?php

namespace IPC\ValidatorBundle\Validator\Constraints;


use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class EqualPropertiesValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @return void
     *
     * @throws AccessException
     * @throws InvalidArgumentException
     * @throws UnexpectedTypeException
     */
    public function validate($value, Constraint $constraint): void
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $values = [];
        if (\is_array($value)) {
            foreach ($constraint->properties as $property) {
                $propertyValue = $accessor->getValue($value, "[$property]");
                if (null === $propertyValue && $constraint->skipNull) {
                    continue;
                }
                $values[] = serialize($propertyValue);
            }
        } elseif (\is_object($value)) {
            foreach ($constraint->properties as $property) {
                $propertyValue = $accessor->getValue($value, $property);
                if (null === $propertyValue && $constraint->skipNull) {
                    continue;
                }
                $values[] = serialize($propertyValue);
            }
        } // no else

        $valueCount = \count(array_unique($values));
        switch ($valueCount) {
            // no properties configured
            case 0:
                $this->context
                    ->buildViolation(EqualProperties::MESSAGE_NO_PROPERTIES)
                    ->addViolation();
                break;
            case 1:
                // all values are equal
                if ($constraint->invert) {
                    $message = $constraint->message ?: EqualProperties::MESSAGE_EQUAL_INVERT;
                    $this->context
                        ->buildViolation($message)
                        ->atPath(reset($constraint->properties))
                        ->addViolation();
                }
                break;
            default:
                // one ore more different values
                if (!$constraint->invert) {
                    $message = $constraint->message ?: EqualProperties::MESSAGE_EQUAL;
                    $this->context
                        ->buildViolation($message)
                        ->atPath(reset($constraint->properties))
                        ->addViolation();
                }
                break;
        }
    }
}
