<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateInFutureValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        if ($value instanceof \DateTimeInterface) {
            $date = clone $value;
            $today = new \DateTime();
            $today->setTime(0, 0, 0);
            if ($date->getTimestamp() >= $today->getTimestamp()) {
                return;
            }
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation()
        ;
    }
}
