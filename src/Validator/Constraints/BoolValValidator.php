<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BoolValValidator extends ConstraintValidator
{
    protected const VALID_BOOL_CHARS = [0, 1, '0', '1'];

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        if (\is_bool($value) || \in_array($value, self::VALID_BOOL_CHARS, true)) {
            return;
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
